var express = require('express');
var path = require('path');
var favicon = require('static-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var routes = require('./routes/index');
var users = require('./routes/users');

//Include JIRA for data feed
var JiraApi = require('jira').JiraApi;
var jira = new JiraApi('https:', 'jira.gji.com.au', 443, 'username_removed', 'password_removed', '2');

//Include geckoboard-push for API
var Geckoboard = require('geckoboard-push');
var gecko = new Geckoboard({api_key: '6a394dc65579947e6ff1b24170ae9cfd'});
var text = gecko.text('58569-131ba7a2-bcb0-4497-8374-af96fb15f21b');
var summary = '';
var project = '';


//Get JIRA data
jira.findIssue('ICTWO-122', function(error, issue) {

	//Send JSON array to Geckoboard
	text.send([
	  {text: issue.fields.project.key + ' - ' + issue.fields.summary, type : 0}
	], function(err, response){
	  console.log('text', response);
	});
});



	
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(favicon());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());
app.use(require('stylus').middleware(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/users', users);

/// catch 404 and forwarding to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

/// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;
